$(document).ready(function() {
    

    // Siwtcher 
    $('.switcher-filter .switch input').on('change', function() {
        if($(this).prop("checked") == true){
            $('.switcher-filter .annual').addClass('active').siblings().removeClass('active');
            $('.billed').show();
        }
        else if($(this).prop("checked") == false){
            $('.switcher-filter .monthly').addClass('active').siblings().removeClass('active');
            $('.billed').hide();
        }
    });

    $('.switcher2 .switch input').on('change', function() {
        if($(this).prop("checked") == true){
            $('.switcher2 .annual').addClass('active').siblings().removeClass('active');
            $('.dollar-mob').show();
        }
        else if($(this).prop("checked") == false){
            $('.switcher2 .monthly').addClass('active').siblings().removeClass('active');
            $('.dollar-mob').hide();
        }
    });

    /// Show Less and More Plans
    var modulesMoreTable = document.querySelector(".modules-more-table-wrapper"),
        modulesLessTable = document.querySelector(".modules-table-wrapper"),
        showMoreBtn = document.querySelector(".show-more"),
        showLessBtn = document.querySelector(".show-less");

    showMoreBtn.addEventListener("click", function () {
        this.style.display = "none";
        modulesLessTable.style.display = "none";
        modulesMoreTable.style.display = "block";
        showLessBtn.style.display = "block";
    });

    showLessBtn.addEventListener("click", function () {
        this.style.display = "none";
        modulesLessTable.style.display = "block";
        modulesMoreTable.style.display = "none";
        showMoreBtn.style.display = "block";
    });

    $(".price-box").innerWidth($(".table-wrapper.table-data .plans-table tr td:last-of-type").innerWidth() - 1 )
    $(window).resize(function(){
        $(".price-box").innerWidth($(".table-wrapper.table-data .plans-table tr td:last-of-type").innerWidth() - 1 )
    })
})