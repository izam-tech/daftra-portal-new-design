$(document).ready(function() {
    // Fixed Header 
    $(window).scroll(function(){
        headerFixed(50);
    });

    // check header when load page
    headerFixed(0);

    function headerFixed(value) {
        if($(this).scrollTop() > value) {
            $('header').addClass('fixed');
            $('.header-fixed').addClass('pd');
        } else {
            $('header').removeClass('fixed')
            $('.header-fixed').removeClass('pd');
        }
    }

    // Hide Show Mega Menu
    $('.bars-icon a, .open-modules').on('click', function() {
        $('.mega-menu').addClass('open');
        $('body').css('overflow-y', 'hidden')
    });

    $('.mega-menu .close-icon').on('click', function() {
        $('.mega-menu').removeClass('open');
        $('body').css('overflow-y', 'auto');
    });

    // Hide Show Industry Menu
    $(".industry-menu-link").on('click', function() {
        $('.industry-menu').addClass('open');
        $('body').css('overflow-y', 'hidden');
    });

    $('.industry-menu .close-icon, .industry-menu .back-icon').on('click', function() {
        $('.industry-menu').removeClass('open')
        $('body').css('overflow-y', 'auto');
    });

})