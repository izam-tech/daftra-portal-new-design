$(document).ready(function() {
    $(".sub-features .features-links a").on('click', function(){ 
        let selector = $(this).attr('id');
        $('.sub-features .feature-t').attr('style', '')
        if(selector) {
            $('html,body').animate({
                scrollTop: $('.'+ selector).offset().top - 70
            });
            $('.' + selector).css({
                'border-left': '4px solid #0000FF',
                'padding-left': '10px'
            });
        }
    });

    function checkOffset() {
        if($('.feature-list').offset().top + $('.feature-list').height() >= $('footer').offset().top - 10)
            $('.feature-list').css({
                'position': 'absolute',
                'bottom': 0,
                'width': '136%'
            });

        if($(document).scrollTop() + window.innerHeight < $('footer').offset().top)
            $('.feature-list').css({
                'position': 'fixed',
                'bottom': 'auto',
                'width': '38%'
            });
        
    }
    
    if($(window).width() > 991) {
        $(document).scroll(function() {
            checkOffset();
        });
    }
});